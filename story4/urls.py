from django.conf.urls import url
from . import views
from django.urls import path


app_name = 'story4'
urlpatterns = [
    path('', views.home, name='index'),
    path('experience', views.exp, name='exp'),
    path('education', views.edu, name='edu'),
    path('contact', views.contact, name='contact'),
    path('addFriend', views.addFriend, name='addfriend'),
    path('Flist', views.Flist, name='flist')
]