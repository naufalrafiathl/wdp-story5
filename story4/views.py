from django.shortcuts import render
from django.http import HttpResponse
from .forms import FriendForm
from .models import Friend

# Create your views here.
def home(request):
    return render(request, 'index.html')

def exp(request):
    return render(request, 'exp.html')

def edu(request):
    return render(request, 'edu.html')

def contact(request):
    return render(request, 'contact.html')

def Flist(request):
    form1 = Friend.objects.all()
    context = {
        'friend' : form1
    }

    return render(request, 'Friendlist.html', {'Friend':form1})

def addFriend(request):
    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
             friend_item = form.save(commit=False)
             friend_item.save()
             return render(request, 'validform.html')
    else:
        form = FriendForm()
    return render(request, 'Form.html', {'form': form})
             