from django.db import models

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=255, default="", blank=False)
    hobby = models.CharField(max_length=255, default="hobby", blank=False)
    year_choices=(('2019', '2019'), ('2018','2018'), ('2017', '2017'), ('2016', '2016'))
    year = models.CharField(choices=year_choices, default='2019', blank=False, null=True, max_length=4)


    def __str__(self):
        return '%s' % self.name